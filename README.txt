README README README

This repo was created to facilitate code-sharing with Marcia Spetch,
Elliot Ludvig and Chris Madan in the context of the Risky Choice 
project (not for further distribution please).

Please also note that some of the code, such as nBackLocalizer, requires
"SuperPsychToolbox" to run - please pull the latest version from
https://bitbucket.org/jpoppenk/superpsychtoolbox

SuperPsychToolbox (SPTB) in turn requires PsychToolbox and Matlab 2014a
or later, and is currently only supported on Linux and MacOSX. Please
make sure that SPTB, RiskyLaunch and the nBackLocalizer folder are all
in the MATLAB path.

You will need to edit this file to update the device names from "???" to
how they appear on the scanner and/or testroom system. To learn the
device names, connect to that system with USB, type "getInputDevice" and
locate the appropriate device name. You will also need to provide device
measurements for screen size/distance in this same script, currently set
to nan.

You'll also need to let me know about the mapping of fingers and the
trigger pulse to numeric values based on your current scanner, if
that does not correspond to Current Design. Also, does the user use the
same device as the trigger arrives on?

To launch the button check, use RiskyLaunch(ppt_id), or you can run in
debug mode with RiskyLaunch(ppt_id,1,1). To launch nback, you can use 
RiskyLaunch(ppt_id,'nback') or RiskyLaunch(ppt_id,2).

To help you become more familiar with SPTB, we have some extensively
commented demos to help introduce you to each function. Please also
consult the extensive documentation in each function with "help
myFunction". We have a work-in-progress API for SPTB that displays
similar information at http://popmem.ddns.net/sptb/api/API.html and are
actively building up our documentation base this summer.

Best of luck and please get in touch with comments or bugs!

Sincerely

Jordan Poppenk
POPMEM Computational Neuroimaging Lab
Queen's University
http://popmem.com
jpoppenk@queensu.ca
