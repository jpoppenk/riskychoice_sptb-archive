function [] = generate_backgrounds(varargin)

% [] = GENERATE_BACKGROUNDS_LOCI1Q(...)
%
% Goes through all the stimuli in directory STIMDIR, loads
% them in as images, creates a scrambled version, and then
% saves it into '%s_sc/%s_c.jpg'.
%
% Based on PicScramble_basic.m, sent by Ehren Newman on
% 090729 (possibly written by Ronnie Bryan).
%
% FILE_EXT (optional, default = .jpg').
%
% STIMDIR (optional, default = pwd).



% defaults.stimdir = pwd;
% defaults.file_ext = '.jpg';
% args = propval(varargin,defaults);
% args_into_workspace
% clear args

stimdir = pwd;

if size(varargin)  > 0
    stimdir = varargin{1};
end

% new_dir = [old_dir '_eq/'];
stimdir = [stimdir '/'];
file_ext = '.jpg';

clear varargin


startdir = pwd;
cd(stimdir);
% i.e. blah/shoe -> blah/shoe/../shoe_sc, i.e. blah/shoe_sc
scramdir = sprintf('%s_sc',pwd);
if ~exist(scramdir,'dir'), mkdir(scramdir), end

files = dir('.');
nFiles = length(dir);
order = randperm(nFiles);
for f=1:nFiles

  cur_file = files(f);
  filen = cur_file.name;
  
  % ignore non-.png files
  if length(filen)<4 || ~strcmp(filen(end-3:end),file_ext)
    continue
  end

  full_filen = fullfile(stimdir,filen);
  pic = double(imread(full_filen));
  pic = trim_dimensions(pic);
  pic_sc = generate_single_background(pic);
  
  % decided not to append _sc to the filename, to make it
  % easier to read in the stimuli
  %
  % file_ext, sprintf('_sc%s',file_ext));
  full_filen_sc = fullfile(scramdir,['scrambled_' num2str(order(f)) file_ext]);
  imwrite(pic_sc,full_filen_sc,'jpg');
  disp(full_filen_sc)

end % f nFiles

save_filen = fullfile(scramdir,'generate_backgrounds_loci1q.mat');
save(save_filen)

% return to original location
cd(startdir);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [pic] = trim_dimensions(pic)

% ensure that all the image dimensions are even (otherwise
% GENERATE_SINGLE_BACKGROUND will crash)

[M, N] = size(pic);
if mod(M,2)
  % chop off the top
  pic = pic(2:end,:);
end
if mod(N,2)
  % chop off the right
  pic = pic(:,1:end-1);
end
% recalculate, in case we did some amputation
[M, N] = size(pic);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [scrambledpic] = generate_single_background(pic)

% [SCRAMBLEDPIC] = GENERATE_SINGLE_BACKGROUND(PIC)
%
% Scrambles a JPG file by adding a random phase
%
% shoe1 = imread('/Users/greg/fmri/distpat/trunk/users/greg/loci1q/stimuli/shoe/shoe1.jpg');
% shoe1 = double(shoe1);
% shoe1_sc = generate_backgrounds_ehren(shoe1);


% get number of samples
[M, N] = size(pic);

% for some reason, this script breaks if the dimensions of
% the image aren't even
if mod(M,2) || mod(N,2)
  error('Image dimensions must be even')
end

% convert image to k space
pic_fft = fft2(pic);

% calculate magnitude of each frequency
vMagnitude = abs(pic_fft);

% scrambled phase -- random except for DC, NyQuist
%disp('Creating scrambled picture...');
picPhase = unwrap(angle(pic_fft));
randomPhase = rand(size(pic_fft)) .* 2 .* pi;
scrambledPhase = randomPhase;
dcIndex = 1;
nyQuistIndexM = M/2 + 1;
nyQuistIndexN = N/2 + 1;

% convert magnitude and scrambled phases to complex numbers
scrambledpic_fft = vMagnitude .* exp(i*scrambledPhase);

% mirror top half into bottom half as complex conjugates
scrambledpic_fft(M/2+2:M,2:N) = scrambledpic_fft(M/2:-1:2,N:-1:2)-2i*imag(scrambledpic_fft(M/2:-1:2,N:-1:2));
% mirror 1st half of middle row into second half
scrambledpic_fft(M/2+1,N/2+2:N) = scrambledpic_fft(M/2+1,N/2:-1:2)-2i*imag(scrambledpic_fft(M/2+1,N/2:-1:2));
% mirror 1st half of first row into second half
scrambledpic_fft(1,N/2+2:N) = scrambledpic_fft(1,N/2:-1:2)-2i*imag(scrambledpic_fft(1,N/2:-1:2));
% mirror 1st half of first column into second half
scrambledpic_fft(M/2+2:M,1) = scrambledpic_fft(M/2:-1:2,1)-2i*imag(scrambledpic_fft(M/2:-1:2,1));


% restore original values of the constants and Nyquist frequency
scrambledpic_fft (1,1) = pic_fft (1,1);
scrambledpic_fft (1,nyQuistIndexN) = pic_fft (1,nyQuistIndexN);
scrambledpic_fft (nyQuistIndexM,1) = pic_fft (nyQuistIndexM,1);
scrambledpic_fft (nyQuistIndexM,nyQuistIndexN) = pic_fft (nyQuistIndexM,nyQuistIndexN);


% inverse fft of scrambled picture
scrambledpic = uint8 (real (ifft2 (scrambledpic_fft)));

% display image
% figure, imagesc(scrambledpic); colormap(gray)

