function [] = compose_image(bgdir,stimdir)

% [] = GENERATE_BACKGROUNDS_LOCI1Q(...)
%
% Goes through all the stimuli in directory STIMDIR, loads
% them in as images, creates a scrambled version, and then
% saves it into '%s_sc/%s_c.jpg'.
%
% Based on PicScramble_basic.m, sent by Ehren Newman on
% 090729 (possibly written by Ronnie Bryan).
%
% FILE_EXT (optional, default = .jpg').
%
% STIMDIR (optional, default = pwd).



% defaults.stimdir = pwd;
% defaults.file_ext = '.jpg';
% args = propval(varargin,defaults);
% args_into_workspace
% clear args

% new_dir = [old_dir '_eq/'];
stimdir = [stimdir '/'];
origdir = pwd;
file_ext = '.jpg';

clear varargin

startdir = pwd;
cd(stimdir);
% i.e. blah/shoe -> blah/shoe/../shoe_sc, i.e. blah/shoe_sc

scramdir = sprintf('%s_bg',pwd);
if ~exist(scramdir,'dir'), mkdir(scramdir), end

files = dir('.');
nFiles = length(dir);
for f=1:nFiles

  cur_file = files(f);
  filen = cur_file.name;
  
  % ignore non-.png files
  if length(filen)<4 || ~strcmp(filen(end-3:end),file_ext)
    continue
  end

  full_filen = fullfile(stimdir,filen);
  pic = double(imread(full_filen));
  bg = double(imread([origdir '/' bgdir '/scrambled_' num2str(f) file_ext]));
  
  mask = double(pic ~= 255);
  inv_mask = double(round(imfilter(1-mask,ones(5,5),'conv')));
  newbg = double(bg .* inv_mask)./10000;
  newpic = double(pic .* mask)./400;
  newpic = double(newpic + newbg);
  
  % decided not to append _sc to the filename, to make it
  % easier to read in the stimuli
  %
  % file_ext, sprintf('_sc%s',file_ext));
  newfilen = fullfile (scramdir,filen);
  imwrite(newpic,newfilen,'jpg');
  disp(newfilen)

end % f nFiles

% return to original location
cd(startdir);
