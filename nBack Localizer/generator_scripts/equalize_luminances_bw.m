function [] = equalize_luminances_bw(varargin)

% [] = EQUALIZE_LUMINANCES_LOCI1Q(...)
%
% Based on PROCESS_IMAGES.M in email from Nick Turk-Browne
% in email on 09072?.
%
% It goes through all the images in OLD_DIR, readjusts the
% contrast (IMADJUST), and then runs ADAPTHISEQ to try and
% make sure that the luminance within each sub-tile of the
% image is about the same, then smooths the boundaries
% between tiles.
%
% Script for local contrast equalization
% Nick Turk-Browne & Sam Norman-Haignere
% Last modified: 7/29/09
%
% OLD_DIR (optional, default = '.'). This is the directory
% containing the images we want to equalize.
%
% NEW_DIR (optional, default = OLD_DIR + '_eq'). This is
% where the equalized images will be placed.
%
% DO_PLOT (optional, default = false).

% set defaults
old_dir = pwd;
do_plot = false;

% defaults.do_plot = false;
% defaults.old_dir = pwd;
% defaults.new_dir = '';
% function missing.  args = propval(varargin,defaults);
% args_into_workspace

if size(varargin)  > 0
    old_dir = varargin{1};
end

new_dir = [old_dir '_eq/'];
old_dir = [old_dir '/'];

clear varargin

startdir = pwd;


% equalization parameters
low_bound = .02;
upper_bound = .98;
if isempty(new_dir)
  new_dir = sprintf('%s_eq',old_dir);
end
tiles = 2;
NBins = 500;
ClipLimit = .01;

% create new folder if it doesn't exist
if ~exist(new_dir,'dir')
  mkdir(new_dir);
end

% get image names
old_files = dir(old_dir);
nOldFiles = length(old_files);

% loop through images and process
for f = 1:nOldFiles
  
  old_filen = old_files(f).name;
    
  % ignore non-image files
  if length(old_filen)<4 || ~strcmp(old_filen(end-3:end),'.bmp')
    continue
  end
  
  % load image
  full_old_filen = fullfile(old_dir, old_filen);
  orig_image = imread(full_old_filen,'bmp');
  % dispf(full_old_filen);

  % convert to intensity image
  if ndims(orig_image)>2
    gray_image = rgb2gray(orig_image);
  else
    gray_image = orig_image;
  end

  % compress the edges
  imad_image = imadjust(gray_image, [low_bound upper_bound], []);
  
  % adjust intensity histogram to uniform distribution
  imad_eq_image = adapthisteq(imad_image, ...
                              'NumTiles', [tiles tiles], ...
                              'NBins', NBins, ...
                              'ClipLimit', ClipLimit);

  % enforce mean-centering around the luminance midpoint
  imad_eq_image = double(imad_eq_image) + (127 - mean(mean(double(imad_eq_image))));
  
  % clip values that are now too high
  imad_eq_image(imad_eq_image<0) = 0; imad_eq_image(imad_eq_image>255) = 255;
  
  % un-invert image
  imad_eq_image = uint8(abs(imad_eq_image - 255));

  % save image
  full_new_filen = fullfile(new_dir, old_filen);
  disp(full_new_filen)

  if do_plot
    % show stages
    figure; imshow(orig_image);    colormap(gray); titlef('orig')
    figure; imshow(imad_image);    colormap(gray); titlef('imad')
    figure; imshow(imad_eq_image); colormap(gray); titlef('imad_eq')
  end
  
  imwrite(imad_eq_image, [full_new_filen(1:end-3) 'jpg'], 'jpg');
end % f nOldFiles

% might as well save the current matlab workspace (mainly
% because it includes info about the old and new
% directories)
% save_filen = fullfile(new_dir,'equalize_luminances_loci1q.mat');
% disp(['Saving to: ' save_filen])
% save(save_filen)

