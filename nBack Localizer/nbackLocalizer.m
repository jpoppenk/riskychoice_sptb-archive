% syntax: nbackLocalizer(params, tokens)
%
% In this experiment, a train of face, scene, car and word images are
% presented successively to participants. When an image is repeated N
% images apart, participants are expected to press a button. This
% experiment is suitable for gathering data to train a category
% localizer.
%
% Written by J. Poppenk on Oct. 5, 2015 at Queen's University

function nbackLocalizer(params,tokens,input_args)  

    %% declarations

    TR_length = input_args{1};
    
    % constants
    HORIZONTAL = 1;
    VERTICAL = 2;

    % parameters
    NBACK = 1; % number of items back to search for matches
    TWO_CAT = true; % face-scene; otherwise face-scene-car-word
    if TR_length < 0
        params.time.tr_len = 2;
        NUM_BLOCKS = 4;
    else
        params.time.tr_len = TR_length;
        NUM_BLOCKS = 10; % should be divisible by four
    end
    PIC_DUR = 0.9*params.session.speed;
    REP_KEY = params.keys.user.index.R;
    ISI_DUR = (1*params.session.speed-PIC_DUR); % we've got 1s to get to the next stimulus
    IBI_DUR = 10*params.session.speed;
    NUM_ITEMS = 10; % num items before repeats are added
    NUM_REPEATS = 6; % num repeats
    TR_TIMEOUT = .1; % grace period allowed while waiting for TR
    params.time.fMRI_stabilization = 14;
    
    % instructions
    INSTRUCT = ['ONE-BACK TASK\n\nIn this task, you will search for repeated rooms, ' ...
        'cars, faces, and words. Please press the ' params.strings.user.index.R ' button if an item is ' ...
        'presented twice in a row.\n\n' params.strings.still_flex params.strings.scan_start];    

    % stimuli
    if TWO_CAT
        CAT_NAMES = {'FACES','SCENES'};
        CAT_FILES = {[params.paths.launch_dir 'bw_faces.txt'],[params.paths.launch_dir 'bw_bedrooms.txt']};
    else
        CAT_NAMES = {'FACES','SCENES','WORDS','CARS'};
        CAT_FILES = {[params.paths.launch_dir 'bw_faces.txt'],[params.paths.launch_dir 'bw_bedrooms.txt'],[params.paths.launch_dir 'bw_words.txt'],[params.paths.launch_dir 'bw_cars.txt']};
    end
    
    % easyKeys prep
    respmap = makeMap({'repeat'}, 1, {REP_KEY});
    condmap = makeMap(CAT_NAMES,[]);
    
    
    %% get materials
    stimlist = [];
    for cat = 1:length(CAT_NAMES)
        loc_stim{cat} = readStimulusFile(CAT_FILES{cat},NUM_ITEMS);
        stimlist = [stimlist loc_stim{cat}];
    end
    stimmap = makeMap(stimlist);

    % make block order with four categories; ensure 1 of each in every multi-block subblock
    block_order = [];
    for i=1:NUM_BLOCKS/length(CAT_NAMES)
        block_order = [block_order randperm(length(CAT_NAMES))];
    end

    % show instructions until trigger is received
    DrawFormattedText(tokens.main_window,INSTRUCT,'center','center',params.appearance.text,params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    WaitSecs(params.time.min_display);
    expStartTime = waitForKeyboard(params.keys.admin.trigger,tokens.user_keys);

    % nag about motion during stabilization period
    DrawFormattedText(tokens.main_window,params.strings.multiband_reminder,'center','center',params.appearance.text,params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    expStartTime2 = waitForKeyboard(params.keys.admin.trigger,tokens.user_keys);
    
    % now show fixation until stabilization is done
    DrawFormattedText(tokens.main_window,'+','center','center',params.appearance.text,params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    
    % initialize
    hit_keys.scan_triggers = 1;
    missed_keys.scan_triggers = 0;
    oneback = initEasyKeys(params.session.sess_name, params.session.ppt_name, ...
                            'session', 1, ...
                            'device', tokens.user_keys, ...
                            'trigger_next', false, ...
                            'default_respmap', respmap, ...
                            'condmap', condmap, ...
                            'stimmap', stimmap, ...
                            'prompt_dur', PIC_DUR, ...
                            'exp_onset', expStartTime2, ...
                            'console', true);
    oneback.exp_params = params;
    oneback = startSession({oneback});
    WaitSecs(params.time.fMRI_stabilization - (GetSecs - expStartTime2));

    % loop over blocks
    for block = 1:length(block_order)
        cat = block_order(block);

        % for this block, make a random item order of unique items, then inject repeats
        item_order = randperm(NUM_ITEMS);
        for i = 1:NUM_REPEATS
            choice = randi(NUM_ITEMS);
            pos = find(item_order==choice,1,'first');
            item_order = [item_order(1:pos) choice item_order(pos+1:end)];
        end

        % sync up at beginning of block
        waited = waitForKeyboard(params.keys.admin.trigger,tokens.user_keys,TR_TIMEOUT);
        if waited < TR_TIMEOUT
            hit_keys.scan_triggers = hit_keys.scan_triggers + 1;
        else missed_keys.scan_triggers = missed_keys.scan_triggers + 1;
        end

        % loop over stimuli within block
        for i = 1:length(item_order)

            % begin with fixation cross
            DrawFormattedText(tokens.main_window,'+','center','center',params.appearance.text,params.appearance.wrap);
            isi_onset = Screen('Flip',tokens.main_window);
            
            % get stim ready
            stim = loc_stim{cat}{item_order(i)};
            img = imread([CAT_FILES{cat}(1:end-4) filesep stim]);
            pic_dims = size(img);
            topLeft(HORIZONTAL) = params.display.center(HORIZONTAL) - (pic_dims(HORIZONTAL)/2);
            topLeft(VERTICAL) = params.display.center(VERTICAL) - (pic_dims(VERTICAL)/2);
            pic_handle = Screen('MakeTexture', tokens.main_window, img);
            Screen('DrawTexture', tokens.main_window, pic_handle, [0 0 pic_dims],[topLeft topLeft+pic_dims]);
            WaitSecs(ISI_DUR - (GetSecs-isi_onset));

            % present stimulus
            onset = Screen('Flip',tokens.main_window);
            Screen('Close',pic_handle);

            % determine correct response, collect actual response and wait if necessary
            if i <= NBACK
                cresp = {nan};
            else
                if item_order(i) == item_order(i-NBACK)
                    cresp = {REP_KEY};
                else cresp = {nan};
                end
            end
            oneback = easyKeys(oneback, 'onset', onset, ...
                                        'cresp', cresp, ...
                                        'stim', stim, ...
                                        'nesting', [block i], ...
                                        'cond', cat);
        end % block items

        % conclide with fixation cross
        DrawFormattedText(tokens.main_window,'+','center','center',params.appearance.text,params.appearance.wrap);
        Screen('Flip',tokens.main_window);

        % we'll try our best to sync up to a TR between blocks
        expDuration = GetSecs() - expStartTime;
        timeout = params.time.tr_len - mod(expDuration, params.time.tr_len) + TR_TIMEOUT;
        waitForKeyboard(params.keys.admin.trigger,tokens.user_keys,timeout);
        
        % wait for next block
        WaitSecs(IBI_DUR);
    end % block

    % wait for end of expt
    WaitSecs(IBI_DUR);
    if strcmp(params.session.control,'offline'), feedback=true; else feedback=false; end
    endSession({oneback},'tokens',tokens,'params',params,'feedback',feedback,'message',params.strings.end_task);
return