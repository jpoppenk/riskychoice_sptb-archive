% syntax: fmriSysTest(params, tokens)
%
% This code checks that the user is able to operate the needed keys on
% their keypad, gives them a few short messages about being in an fMRI
% scan, and then exits.
%
% Written by J. Poppenk on Oct. 5, 2015 at Queen's University

function fmriSysTest(params, tokens, input_args)
    
    % assign inputs
%    needed_keys = input_args{1};

    % check that we are online
    if strcmp(params.keys.user.exit,'Return')
        if params.session.debug_mode
            warning(['fMRI setup test error: we do not seem to be in fMRI mode! ' ...
                'We are, however, in debug mode, so we will continue. Tread carefully.'])
        else
            error('fMRI setup test error: we do not seem to be in fMRI mode!')
        end
    end
    
    % welcome!
    DrawFormattedText(tokens.main_window,'Welcome to your fMRI scanning session!\n\nOnce you''re all the way inside the scanner and can read this text, please reach up to your eyes and fine-tune the position of your mirror. You want to set it so you can see as much of the screen as comfortably as possible. This will be your last chance to adjust your mirror, so be sure to set it just so.\n\nOnce you''ve adjusted the mirror to your satisfaction, please press the index finger button to test your button pad.','center','center',params.appearance.text,params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    waitForKeyboard(params.keys.user.index.R,tokens.user_keys);

    % index finger not a fluke
    DrawFormattedText(tokens.main_window,'Great. I detected that button press, which means at least one button works. Now let''s try the rest of them. Press the index button to continue.','center','center',params.appearance.text,params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    waitForKeyboard(params.keys.user.index.R,tokens.user_keys);

    % middle finger
    DrawFormattedText(tokens.main_window,'Please press the middle finger button...','center','center',params.appearance.text,params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    waitForKeyboard(params.keys.user.middle.R,tokens.user_keys);

    % ring finger
    DrawFormattedText(tokens.main_window,'And now the ring finger button...','center','center',params.appearance.text,params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    waitForKeyboard(params.keys.user.ring.R,tokens.user_keys);
    
    % pinky finger
    DrawFormattedText(tokens.main_window,'And now the pinky finger button...','center','center',params.appearance.text,params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    waitForKeyboard(params.keys.user.pinky.R,tokens.user_keys);
    
    % works, squeeze ball instruct
    DrawFormattedText(tokens.main_window,['Good news! It looks like the button pad is working ' ...
        'just fine, but please let us know if it was hard to get any of those buttons to work.' ...
        '\n\nAlso, just a reminder that we can hear your voice when the scanner is ' ...
        'at rest, so just speak up to ask or tell us something. During a scan, we will ' ...
        'abort right away if you use the squeeze ball, but please do so only if ' ...
        'there''s something urgent we need to address immediately.\n\n-- please press ' ...
        'the index finger button to continue --'],'center','center',params.appearance.text,params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    waitForKeyboard(params.keys.user.index.R,tokens.user_keys);
    
    % no loops in body
    DrawFormattedText(tokens.main_window,['During the scan today, please try to avoid forming any ' ...
        '''loops'' with your body. That is, let your arms rest at your side, and do ' ...
        'not cross your legs. There is a chance if you do allow ''loops'' to form with ' ...
        'your body, you could experience uncomfortable heating up as a result of the ' ...
        'physics of the MRI machine.\n\nAlso, please be advised that some participants ' ...
        'experience a tingling sensation during the scan. Don''t be concerned if you ' ...
        'experience this, it is entirely safe and normal.\n\n-- please press the index ' ...
        'finger button to continue --'],'center','center',params.appearance.text,params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    waitForKeyboard(params.keys.user.index.R,tokens.user_keys);
    
    % thanks, ttys
    DrawFormattedText(tokens.main_window,['We''re going to need a few minutes to calibrate ' ...
        'the scanner. You''ll hear some knocking noises; just ignore them.\n\nWe''ll ' ...
        'be in voice contact with you soon. Thanks for your patience!\n\n-- please press ' ...
        'the index finger button to continue --'], 'center','center',params.appearance.text, ...
        params.appearance.wrap);
    Screen('Flip',tokens.main_window);
    WaitSecs(params.time.basic_wait);

return