% This launcher function has been set up to run superpsychtoolbox
% experiments for Risky Choice project in Alberta. Written by J. Poppenk on
% June 10, 2016

function RiskyLaunch(ppt_ID, start_at, speed)
    
    %% parameter settings: update this section for your experiment

    % known device ID's
    queens_fMRI.current_design = 16385;
    console.keyboard = '???';
    console.mouse = '???';
    testroom.keyboard = '???';
    testroom.mouse = '???';
    
    % device selection
    device_keyboard = {console.keyboard, testroom.keyboard};
    device_mouse = {console.mouse, testroom.mouse};
    device_fMRI = {queens_fMRI.current_design};
    device_audio = {};

    % session map: first item in each cell is a string describing the
    % experiment function to be called (used in producing data filenames).
    % the second item is the call to that function. The third is a code
    % (specified below) to designate how device control of that session
    % should be handled.
    session_data = { ...
            {'Button check', 'fmriSysTest', {'12345'}}, ...
            {'nBack', 'nbackLocalizer', {2}}, ...
            };
        
        
    %% check inputs and launch (avoid modifications below this line)
    
    % Check for complete input
    if ~exist('ppt_ID','var') || isempty(ppt_ID) || ...
        (exist('start_at','var') && isempty(start_at))
        easyLaunch([],session_data)
        disp('use valid session and syntax: (ppt_ID [, start_at] [, optional speed multiplier])')
    else
        % check for missing values
        if ~exist('start_at','var'), start_at = 1; end
        if ~exist('speed','var'), speed = []; end
        
        % this is a session map: use single-shot mode
        end_at = 'single';
        
        % physical setup
        display_dist = nan;
        display_width = nan;
        display_height = nan;

        % launch battery using expLauncher
        easyLaunch(ppt_ID,session_data,'start_at', start_at, ...
                                        'end_at', end_at, ...
                                        'speed', speed, ...
                                        'keyboard', device_fMRI, ...
                                        'mouse', device_mouse, ...
                                        'audio', device_audio, ...
... %                                   'admin_input', device_keyboard, ...
                                        'display_dist', display_dist, ...
                                        'display_height', display_height, ...
                                        'display_width', display_width, ...
                                        'fMRI', 'current_design');
    end
return