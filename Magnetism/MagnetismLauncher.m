% specify just_completed to resume the experiment from the task that
% follows that one
function MagnetismLauncher(ppt_ID, start_at, speed)
    
    %% parameter settings: update this section for your experiment

    % known device ID's
    queens_mock_scan.current_design = 'curdes.com fORP Interface';
    queens_mock_scan.keyboard = 'Logitech USB Keyboard';
    queens_mock_scan.mouse = 'Logitech USB Optical Mouse';
    queens_fMRI.current_design = 16385;
    popmem_test_rooms.keyboard = 'Razer Razer Blackwidow Tournament Edition';
    popmem_test_rooms.mouse = 'Razer Razer Abyssus';
    popmem_workstation.keyboard = 'Apple Keyboard';
    popmem_workstation.mouse = 'USB Laser Mouse';
    apple_laptop.keyboard = 'Apple Internal Keyboard / Trackpad';
    apple_laptop.mouse = 'Apple Internal Keyboard / Trackpad';
    jordans_office.keyboard = 'daskeyboard';
    jordans_office.mouse = 'USB Laser Mouse';
    
    % device selection
    device_keyboard = {popmem_test_rooms.keyboard, jordans_office.keyboard, apple_laptop.keyboard, popmem_workstation.keyboard, queens_mock_scan.keyboard};
    device_mouse = {popmem_test_rooms.mouse, jordans_office.mouse, apple_laptop.mouse, popmem_workstation.mouse, queens_mock_scan.mouse};
    device_audio = {};
    
    % session map: first item in each cell is a string describing the
    % experiment function to be called (used in producing data filenames).
    % the second item is the call to that function. The third is a code
    % (specified below) to designate how device control of that session
    % should be handled.
    session_data = { ...
            ... % study phase
            {'Magnetism practice', 'magnetism', {'study practice',1}}, ... % 5 items = .5 mins
            {'Magnetism', 'magnetism', {'study',1:12}}, ... % 20items * 12blocks * 6secs = 24 mins
            ...
            ... % recognition memory for objects shown earlier
            {'Recognition Practice', 'magnetism', {'recog practice',1}}, ... % 5 items = .5 mins
            {'Recognition', 'magnetism', {'recog',1}}, ... % 20items * 6secs = 2 mins
            ...
            ... % memory for paired object
            {'MultiObject Practice', 'magnetism', {'multiobject practice',1}}, ... % 5 items = .5 mins
            {'MultiObject', 'magnetism', {'multiobject',2}}, ... % 20items * 6secs = 2 mins
            ...
            ... % drag into position (overall memory)
            {'Dragging Items Practice', 'magnetism', {'drag item practice',1}}, ... % 5 items = .5 mins
            {'Dragging Items', 'magnetism', {'drag item',3}}, ... % 20items * 6secs = 2 mins
            {'Dragging Cued Items', 'magnetism', {'drag item cued',4}}, ... % 20items * 6secs = 2 mins
            {'Dragging Targets', 'magnetism', {'drag targonly',5}}, ... % 20items * 13secs = 4.333 mins
            ...
            ... % select objects from various possible positions
            {'MultiChoice Practice', 'magnetism', {'multichoice practice',1}}, ... % 5 items = .5 mins
            {'MultiChoice', 'magnetism', {'multichoice',6}}, ... % 20items * 6secs = 2 mins
            ...
            ... % determine whether objects have flipped positions
            {'Flip Practice', 'magnetism', {'config practice',1}}, ... % 5 items = .5 mins
            {'Flip', 'magnetism', {'config',7}}, ... % 20items * 6secs = 2 mins
            ...
            ... % put it all together
            {'Dragging Composite Practice', 'magnetism', {'drag composite practice',1}}, ... % 5 items = 1.5 mins
            {'Dragging Composite', 'magnetism', {'drag composite',8}}, ... % 20items * 13secs = 4.333 mins
            ...
            ... % drag around items in a fixed configuration relative to one another
            {'Dragging Global Practice', 'magnetism', {'drag global detail practice',1}}, ... % 5 items = .5 mins
            {'Dragging Global', 'magnetism', {'drag global detail',9}}, ... % 20items * 6secs = 2 mins
            ...
            ... % items are close to position and need only slight repositioning
            {'Dragging Local Practice', 'magnetism', {'drag local detail',1}}, ... % 5 items = .5 mins
            {'Dragging Local', 'magnetism', {'drag local detail',10}}, ... % 20items * 6secs = 2 mins
            {'Dragging Item Detail', 'magnetism', {'drag item detail',11}}, ... % 20items * 6secs = 2 mins
            ...
            ... % dragging while being shown where everything goes (no memory)
            {'Dragging Motor Practice', 'magnetism', {'motor practice',1}}, ... % 5 items = 1.5 mins
            {'Dragging Motor', 'magnetism', {'motor',12}}, ... % 20items * 13secs = 4.333 mins
            ...
            ... %% REVERSED ORDER FOR COUNTERBALANCING
            {'Magnetism 2', 'magnetism', {'study',13:24}}, ...
            {'Dragging Motor 2', 'magnetism', {'motor',13}}, ...
            {'Dragging Item Detail 2', 'magnetism', {'drag item detail',14}}, ...
            {'Dragging Local 2', 'magnetism', {'drag local detail',15}}, ...
            {'Dragging Global 2', 'magnetism', {'drag global detail',16}}, ...
            {'Dragging Composite 2', 'magnetism', {'drag composite',17}}, ...
            {'Flip 2', 'magnetism', {'config',18}}, ...
            {'MultiChoice 2', 'magnetism', {'multichoice',19}}, ...
            {'Dragging Targets 2', 'magnetism', {'drag targonly',20}}, ...
            {'Dragging Cued Items 2', 'magnetism', {'drag item cued',21}}, ...
            {'Dragging Items 2', 'magnetism', {'drag item',22}}, ...
            {'MultiObject 2', 'magnetism', {'multiobject',23}}, ...
            {'Recognition 2', 'magnetism', {'recog',24}}};

            
    %% check inputs and launch (avoid modifications below this line)
    
    % Check for complete input
    if ~exist('ppt_ID','var') || isempty(ppt_ID) || ...
        (exist('start_at','var') && isempty(start_at))
        easyLaunch([],session_data)
        disp('use valid session and syntax: (ppt_ID [, start_at] [, optional speed multiplier])')
    else
        % check for missing values
        if ~exist('start_at','var'), start_at = 1; end
        if ~exist('speed','var'), speed = []; end
        
        % this is a session map: use single-shot mode
        end_at = 'auto';
        
        % physical setup
        display_dist = 31;
        display_width = 16;
        display_height = 12;

        % launch battery using expLauncher
        easyLaunch(ppt_ID,session_data,'start_at', start_at, ...
                                        'end_at', end_at, ...
                                        'speed', speed, ...
                                        'keyboard', device_keyboard, ...
                                        'mouse', device_mouse, ...
                                        'audio', device_audio, ...
                                        'display_dist', display_dist, ...
                                        'display_height', display_height, ...
                                        'display_width', display_width);
    end
return