function eS = magnetism(params,tokens,input_args)

    %% parameters
    exp_name = 'magnetism';
    session = input_args{1};
    block = input_args{2};
    max_targs_per_scene = 2;
    targs_per_scene = 2;
    max_lures_per_scene = 2;
    max_options_per_trial = 5;
    max_multichoice_options = 4;

    % experiment params
    global_var = .1;
    local_var = .03;
    max_blocks = 24;
    num_scenes_practice = 5;
    num_scenes_per_block = 20;
    famil_lures_per_scene = 1; % can't be larger than targs_per_scene
    novel_lures_per_scene = 1;
    study_dur = 5 * params.session.speed;
    recog_dur = 2.5 * params.session.speed;
    recog_listen_dur = .9 * params.session.speed;
    recog_isi_dur = 2.5 * params.session.speed;
    multiobject_dur = 5 * params.session.speed;
    multiobject_listen_dur = 0 * params.session.speed;
    multiobject_isi_dur = .75 * params.session.speed;
    config_dur = 5 * params.session.speed;
    item_test_dur = 5 * params.session.speed;
    test_dur = 12 * params.session.speed;
    isi_dur = .75 * params.session.speed;
    font_diff = -4;
    magnet_resps = {'z','v','m'};
    config_resps = {'s','u'};
    multiobject_resps = {'1','2','3','4','5'};
    multiobject_resps = multiobject_resps(1:max_options_per_trial);
    multiobject_labels = {'Position 1','Position 2','Position 3','Position 4','Position 5'};
    multiobject_labels = multiobject_labels(1:max_options_per_trial);
    recog_resps = {'r','k','n'};
    magnet_vals = [-1 0 1];
    recog_vals = [2 1 0];
    config_vals = [1 0];
    magnet_labels = {'closer','same','further'};
    config_labels = {'switched', 'unswitched'}; %{'sure correct','unsure correct','unsure switched','sure switched'};
    recog_labels = {'remember','know','new'};
    empty_stem = [params.paths.resource_dir 'EmptySquares' filesep];
    empty_sq = {[empty_stem '1.png'],[empty_stem '2.png'],[empty_stem '3.png'], ...
                    [empty_stem '4.png'],[empty_stem '5.png']};
    multichoice_resps = {'1','2','3','4','5'};
    multichoice_resps = multichoice_resps(1:max_options_per_trial);
    multichoice_labels = {'Position 1','Position 2','Position 3','Position 4','Position 5',};
    multichoice_labels = multichoice_labels(1:max_options_per_trial);
    
    % drawing params
    grid_colour = 225;
    trial_area = [.1 0 1 1];
    obj_column = .075;
    dashes = .075;
    thresh = .2;
    
    % process session type
    if ~isempty(strfind(lower(session),'practice')),
        suffix = ' PRACTICE';
        recovery = false;
        total_items = num_scenes_practice;
        end_task = params.strings.end_practice;
    else
        suffix = [];
        recovery = true;
        total_items = num_scenes_per_block;
        end_task = params.strings.end_task;
    end
    sess_title = upper(params.session.sess_name);
    if ~isempty(strfind(lower(session),'2')),
        sess_num = 2;
    else sess_num = 1;
    end
    
    % instructions    
    instruct_dir = [params.paths.resource_dir 'Instructions' filesep];
    instructStudy1 = [sess_title '\n\nImage a world where related ' ...
                'objects like to sit close to each other, and unrelated ' ...
                'objects prefer to be far apart.\n\nWe are going to show ' ...
                'you pairs of objects from this world. You will need to ' ...
                'decide, based on their level of relatedness and current ' ...
                'distance, whether they should be closer (press "' magnet_resps{1} ...
                '"), stay the same distance (press "' magnet_resps{2} '"), or be ' ...
                'further apart (press "' magnet_resps{3} '"). You''ll have ' ...
                num2str(study_dur) ' seconds to make your choice.'];
    instructStudy2 = ['For example, imagine that a fish and a car appeared next ' ...
                'to each other. Because they are close but unrelated, you ' ...
                'would press "' magnet_resps{3} '" to indicate they should be ' ...
                'further apart.\n\nConversely, if a banana and a fruit basket ' ...
                'appeared far from each other, you would likely select "' magnet_resps{1} ...
                '" to indicate they should be closer. But if they had appeared ' ...
                'right next to each other, you would likely have selected "' ...
                magnet_resps{3} '". This because although they are same objects ' ...
                'across the two examples, their similarity is not high enough ' ...
                'to justify the short distance in the latter case.'];
    instructStudyGraphic = [instruct_dir 'magnetism_instructs.jpg'];
    instructFlip1 = [sess_title '\n\nWe are now going ' ...
                'to show you the sets of objects you saw earlier; however, half ' ...
                'of the time, the position of two objects will have been switched. ' ...
                'Accordingly, for each object set, please indicate whether the ' ...
                'object positions are switched (press "s") or are unswitched / ' ...
                'normal (press "u").'];
    instructFlipGraphic= [instruct_dir 'flip_instructs.jpg'];
    instructMultichoiceSpace1 = [sess_title '\n\nWe are now going ' ...
                'to show you one of the objects from each pair you saw earlier. ' ...
                'We will also show you various options as to where the object could ' ...
                'have originally belonged. For each object pair, please indicate ' ...
                'which of these possible options is correct by pressing the ' ...
                'corresponding key.'];
    instructMultichoiceSpaceGraphic= [instruct_dir 'config_instructs.jpg'];
    instructMultichoiceObject1 = [sess_title '\n\nWe are now going ' ...
                'to show you one of the objects from each pair you saw earlier. ' ...
                'We will also show you various other objects that might have been' ...
                'originally paired with it. For each object, please indicate ' ...
                'which of these possible options is correct by pressing the ' ...
                'corresponding key.'];
    instructMultichoiceObjectGraphic = [instruct_dir 'pairing_instructs.jpg'];     
    instructMotor1 = [sess_title '\n\nIn this section you don''t need ' ...
                'to remember anything; we will simply show you which objects ' ...
                'need to go where by placing a faded copy at their destination. ' ...
                'We would like you to move objects to their destinations as ' ...
                'quickly and accurately as possible.'];
    instructMotorGraphic = [instruct_dir 'drag_motor_instructs.jpg'];
    instructRecog1 = [sess_title '\n\nYou will now be shown the ' ...
                'same objects you saw a moment ago, plus some new ones. For each ' ...
                'object, please indicate whether you "remember" seeing it (press "' ...
                recog_resps{1} '"), whether you "know" you saw it (press "' recog_resps{2} ...
                '"), or whether it is new (press "' recog_resps{3} '").\n\nThese ' ...
                'meanings are the same as yesterday: "Remembering" concerns your ' ...
                'conscious awareness of some detail of your experience first seeing ' ...
                'the object (e.g., a thought you had, or you heard someone sneeze). ' ...
                'By contrast, "knowing" reflects recognition that you saw the object, ' ...
                'but you cannot consciously recollect anything about seeing its ' ...
                'actual occurrence.'];
    instructRecogGraphic = [instruct_dir 'recog_instructs.jpg'];
    instructDetail1 = [sess_title '\n\nWe''ve placed the ' ...
                'objects roughly where they started, but they are still slightly ' ...
                'misplaced in terms of their position relative to one another. ' ...
                'Your task is now to move each of the objects into exactly the spot ' ...
                'spot where they appeared earlier.'];
    instructDetailGraphic = [instruct_dir 'local_drag_instructs.jpg'];
    instructGlobalDetail1 = [sess_title '\n\nWe''ve placed the ' ...
                'objects roughly where they started, but they are still slightly ' ...
                'misplaced. The relative position of each member of the pair is ' ...
                'correct, but the pair is sligtly misplaced on the screen. When ' ...
                'you drag the pair of objects, they will now move together. Your task ' ...
                'is now to move the pair into exactly the spot where it appeared earlier.'];
    instructGlobalGraphic = [instruct_dir 'global_drag_instructs.jpg'];
    instructCompositeTest1 = [sess_title '\n\nYou will now see pairs ' ...
                'of objects that went together earlier, plus ones that didn''t ' ...
                'go with them. Your job is to drag the objects that went together ' ...
                'into the position where you saw them the first time, while ' ...
                'leaving the mismatched objects behind. You should be as ' ...
                'exact as you possibly can. Make a guess even if you have no ' ...
                'idea, as you may still come close by chance. You have ' num2str(test_dur) ' ' ...
                'seconds per trial to do this.'];
    instructCompositeGraphic = [instruct_dir 'drag_composite_instructs.jpg'];
    instructItem1 = [sess_title '\n\nIn each trial, you will ' ...
                'see an object from earlier in the left-hand column. Position ' ...
                'the object where you originally saw it, ' ...
                'while being as exact as you possibly can. Make a guess even if you ' ...
                'have no idea, as you may still come close by chance. You have ' ...
                num2str(item_test_dur) ' seconds per trial to do this.'];    
    instructItemGraphic = [instruct_dir 'drag_item_instructs.jpg'];
    instructItemCued1 = [sess_title '\n\nIn each trial, you will ' ...
                'see an object from earlier in the left-hand column. To help ' ...
                'jog your memory, he object that it was originally paired with ' ...
                'will already be in position. Position the object where you originally saw it, ' ...
                'while being as exact as you possibly can. Make a guess even if you ' ...
                'have no idea, as you may still come close by chance. You have ' ...
                num2str(item_test_dur) ' seconds per trial to do this.'];    
    instructItemCuedGraphic = [instruct_dir 'drag_item_cued_instructs.jpg'];
    instruct2targs1 = [sess_title '\n\nIn each trial, you will ' ...
                'see two objects from earlier in the left-hand column. Position ' ...
                'both objects where you originally saw them, while being as exact ' ...
                'as you possibly can. You have ' num2str(item_test_dur*2) ' seconds ' ...
                'per trial to do this. Make a guess even if you have no idea, as you ' ...
                'may still come close by chance.'];
    instruct2targsGraphic = [instruct_dir 'drag_2_targs_instructs.jpg'];
    short_study_instructs = ['\nBased on their relatedness and distance, should the objects be closer or further?\n\n' ...
                'Z=closer\nV=same\nM=further'];
    short_recog_instructs = ['\nRemember, know or new?\n\nR=remember\nK=know\nN=new'];
    short_config_instructs = ['\nObject positions switched or unswitched?\n\n' ...
                'S=switched\nU=unswitched'];
    short_multichoiceObject_instructs = ['Which of the objects below was paired with the one above?'];
    short_test_instructs = ['\nDrag the 2 objects below that appeared together to ' ...
                            'their original positions (guess if you don''t know).'];
    short_item_instructs = ['\nDrag the object(s) below back to where they started (guess if you don''t know).'];
    short_motor_instructs = ['\nDrag objects at left to their indicated destinations at right.'];
    short_multichoice_instructs = ['\nPress the key from the box where the object below was originally located (guess if unsure).'];
    short_detail_instructs = ['\nObjects are roughly where they used ' ...
                'to be. Drag them to their exact original positions.'];
    
            
    %% initialize
    
    % initialize geometry
    margin = 50;
    canvas_rect = [0 0 params.display.pixels];
    trial_area_rect = [params.display.pixels.*trial_area(1:2) params.display.pixels .* trial_area(3:4)];
    trial_area_pix = trial_area_rect(3:4)-trial_area_rect(1:2);
    objs_per_scene = targs_per_scene + novel_lures_per_scene + famil_lures_per_scene;
    obj_size = [min(trial_area_pix) min(trial_area_pix)]/(objs_per_scene+1);
    obj_size_half = obj_size/2;
    obj_buffer_pix = obj_size(1);
    test_obj_start_row = params.display.pixels(2) / (objs_per_scene+2); 
    test_obj_start_row = ((2:objs_per_scene+1) * test_obj_start_row);
    test_obj_start_col = round(obj_column * params.display.pixels(1));
    test_obj_drag_line = test_obj_start_col * 2;
    dash_gap = round(min(dashes * params.display.pixels));
    margin = max([margin obj_size]);
    safe_pix = trial_area_pix-(margin*2); % rectangle bounding the drawing area
    global_var = global_var * min(params.display.pixels);
    local_var = local_var * min(params.display.pixels);
    
    % prepare breaks
    study_break_dur = 10 * params.session.speed;
    study_block_size = 40;
    config_break_dur = 'space';
    config_block_size = 40;
    drag_break_dur = 'space';
    drag_block_size = 40;
    recog_break_dur = 'space';
    recog_block_size = 40;
    multiobject_break_dur = 'space';
    multiobject_block_size = 40;
    
    % other initialization
    num_scenes = num_scenes_practice + num_scenes_per_block*max_blocks;
    magnet_respmap = makeMap(magnet_labels,magnet_vals,magnet_resps);
    recog_respmap = makeMap(recog_labels,recog_vals,recog_resps);
    config_respmap = makeMap(config_labels,config_vals,config_resps);
    multichoice_respmap = makeMap(multichoice_labels,[],multichoice_resps);
    multiobject_respmap = makeMap(multiobject_labels,[],multiobject_resps);
    input_func = 'inputFunc_object';
    draw_func = 'drawFunc_object';
    score_func = 'objectScoreFunc';
    allow_mouse_exit = false; % mouse position won't end the trial
    use_os_cursor = false; % use internal cursor
    strict_edges = true; % don't allow cursor to move beyond edges
    input_func_args = {tokens.main_window, canvas_rect, allow_mouse_exit, use_os_cursor, strict_edges};
    draw_func_args = {canvas_rect, use_os_cursor};
    score_func_args = {};
    multichoice_ref_pos = [test_obj_start_col test_obj_start_row(1)];
    multichoice_ref_pos = [multichoice_ref_pos-obj_size_half multichoice_ref_pos+obj_size_half];
    
    % scene position initialization
    if ~isempty(strfind(lower(session),'practice'))
        all_scenes = 1:num_scenes_practice;
        feedback = false;
    else
        % get scenes from all included stimulus blocks
        all_scenes = [];
        for b = 1:length(block)
            start_scene = 1 + num_scenes_practice + (num_scenes_per_block*(block(b)-1));
            end_scene = start_scene + num_scenes_per_block - 1;
            all_scenes = [all_scenes start_scene:end_scene];
        end
        feedback = true;
    end
        
    % prepare the background
    canvas_img = ones(fliplr(params.display.pixels)) * 255;
    for y = 1:dash_gap:size(canvas_img,1)
        canvas_img(y,:) = grid_colour;
    end
    for x = 1:dash_gap:size(canvas_img,2)
        canvas_img(:,x) = grid_colour;
    end
    canvas_img(:,1:test_obj_drag_line) = 25;
    column_rect = [0 0 test_obj_drag_line size(canvas_img,2)];
    col_wrap = round(column_rect(4)/(params.appearance.size*2.9));
    
    
    %% stimulus allocation
    
    % prepare stimuli

    setRandSeed('experiment', exp_name);
    stim_dir = [params.paths.resource_dir 'BradyObjs' filesep];
    input = dir(stim_dir);
    input = {input(:).name};
    find_good_objs = @(x) ~strcmp(x(1),'.') && ~isempty(strfind(lower(x),'.jpg'));
    good_objs = Shuffle(input(cellfun(find_good_objs,input)));
    
    % allocate new stimuli
    counter = 0; all_stim = {}; stim_vals = []; targ_sq = []; scene_novel_vals = [];
    for s = 1:num_scenes
        for o = 1:max_targs_per_scene
            counter = counter + 1;
            scene_objs{s}{o} = [stim_dir good_objs{counter}];
            all_stim{end+1} = [stim_dir good_objs{counter}];
            stim_vals(end+1) = s*10 + o;
        end
        for o = 1:max([novel_lures_per_scene max_targs_per_scene])
            counter = counter + 1;
            scene_novel_lures{s}{o} = [stim_dir good_objs{counter}];
            all_stim{end+1} = [stim_dir good_objs{counter}];
            scene_novel_vals(end+1) = s*10 + o + 10000;
        end            
    end
    
    % allocate familiar lures
    targ_stim = [scene_objs{:}]; targ_vals = stim_vals;
    target_grab_bin = Shuffle([scene_objs{all_scenes}]);
    famil_counter = 0;
    scene_famil_lures = cell(num_scenes,1);
    for s = all_scenes
        for l = 1:max_lures_per_scene
            famil_counter = famil_counter + 1;
            
            % make sure we're not assigning a target from the same trial
            while any(strcmp(scene_objs{s},target_grab_bin{famil_counter}))
                if any(strcmp(scene_objs{s},target_grab_bin{famil_counter}))
                    target_grab_bin = Shuffle(target_grab_bin(famil_counter:end));
                    famil_counter = 1;
                end
            end
            scene_famil_lures{s}{l} = target_grab_bin{famil_counter};
        end
    end
    
    % combine novel and familiar lures
    for s = all_scenes
        scene_drag_lures{s} = [scene_famil_lures{s}(1:famil_lures_per_scene) scene_novel_lures{s}];
        scene_multichoice_lures{s} = [scene_famil_lures{s}(1:2) scene_novel_lures{s}(1:2)];
    end

    % generate object xy positions
    needed_squares = max_lures_per_scene + max_multichoice_options;
    for s = 1:num_scenes
        dists = 0;
        while any(dists < obj_buffer_pix)
            obj_pos_tmp = bsxfun(@times,rand(needed_squares,2),safe_pix);
            dists = min(ipdm(obj_pos_tmp) + eye(needed_squares)*1000000);
            squares_tmp = [bsxfun(@minus,obj_pos_tmp,obj_size_half) ...
                          bsxfun(@plus,obj_pos_tmp,obj_size_half)] + ...
                          repmat(trial_area_rect(1:2),needed_squares,2) + margin;
        end
        obj_pos{s} = mat2cell(obj_pos_tmp(1:max_targs_per_scene,:),ones(max_targs_per_scene,1),2);
        squares{s} = mat2cell(squares_tmp(1:max_targs_per_scene,:),ones(max_targs_per_scene,1),4);
        lure_pos{s} = mat2cell(obj_pos_tmp(max_targs_per_scene+1:max_targs_per_scene+max_lures_per_scene,:),ones(max_lures_per_scene,1),2);
        lure_squares{s} = mat2cell(squares_tmp(max_targs_per_scene+1:max_targs_per_scene+max_multichoice_options,:),ones(max_multichoice_options,1),4);
        targ_sq = [targ_sq; squares{s}];
        obj_pos{s} = obj_pos{s}(1:targs_per_scene);
        squares{s} = squares{s}(1:targs_per_scene);
        lure_obj_pos{s} = lure_pos{s}(1:max_lures_per_scene);
        lure_squares{s} = lure_squares{s}(1:max_multichoice_options);
    end
    
    % log the stimulus allocation
    stimmap_targs = makeMap(targ_stim,targ_vals,targ_sq);
    stimmap_novel = makeMap([scene_novel_lures{:}],scene_novel_vals);
    all_stim = [stimmap_targs(:,1:2); stimmap_novel];
    current_targs = [scene_objs{all_scenes}];
    current_novel = [scene_novel_lures{all_scenes}];

    % pack up trial bin
    trial_bin{1} = which('default_cursor.png');
    trial_bin{2} = canvas_img;
    
    % assign the correct trial duration for the session type
    switch session
        case {'drag item','drag item practice','drag item cued','drag item cued practice','drag local detail', 'drag global detail','drag global detail practice','drag item detail'}
            test_dur = item_test_dur;
    end


    % session switch
    switch session

        %% study section
        case {'study', 'study practice'}

            % initialize
            eK_study = initEasyKeys([params.session.sess_name ' - study'], params.session.ppt_name, ...
                              'session', sess_num, ...
                              'num_trials', total_items, ...
                              'trigger_next', false, ...
                              'prompt_dur', study_dur, ...
                              'default_respmap', magnet_respmap, ...
                              'stimmap', all_stim, ...
                              'default_cresp', {}, ...
                              'device', tokens.user_keys);    
            [eK_study] = startSession({eK_study}, ...
                            'instruct',{instructStudy1, instructStudy2, instructStudyGraphic}, ...
                            'info', {stimmap_targs, stimmap_novel}, ...
                            'seed', 'session', ...
                            'recovery_mode', recovery, ...
                            'params',params,'tokens',tokens);
            all_scenes = Shuffle(all_scenes);
            if ~isempty(eK_study.trials)
                start_scene = eK_study.trials.nesting{end};
                all_scenes = all_scenes(find(start_scene==all_scenes,1,'first'):end);
            end
            trial_bin{4} = {short_study_instructs,column_rect,col_wrap};
            sample_bin{1}{3} = 1:targs_per_scene;

            % loop over study trials
            t_counter = 0;
            for t = all_scenes
                t_counter = t_counter + 1;
                Screen('TextSize', tokens.main_window, params.appearance.size);
                isi(tokens.main_window, isi_dur, params.appearance.text);
                
                % get stim id
                stim_id = stimmap_targs.values(t);
                
                % take a break now and then
                relax(params, tokens, study_block_size, t_counter, study_break_dur);

                % call object draw func
                sample_bin{1}{1} = makeMap(scene_objs{t}(1:targs_per_scene), [], squares{t});
                Screen('TextSize', tokens.main_window, params.appearance.size+font_diff);
                drawFunc_object(tokens.main_window, [], draw_func_args, trial_bin, sample_bin);
                onset = Screen('Flip', tokens.main_window);

                % wait for response
                eK_study = easyKeys(eK_study, ...
                                        'stim', num2str(t), ...
                                        'onset', onset, ...
                                        'info', {scene_objs{t}(1:targs_per_scene), squares{t}}, ...
                                        'nesting', [block t_counter]);
            end % study trials
            if max(block) == max_blocks, feedback = true; else feedback = false; end
            endSession({eK_study},'tokens',tokens,'params',params,'feedback',feedback,'message',end_task);

            
            
        %% recognition section
        case {'recog', 'recog practice'}
            
            % initialize
            eK_recog = initEasyKeys([params.session.sess_name ' - recog'], params.session.ppt_name, ...
                              'session', sess_num, ...
                              'num_trials', total_items*4, ...
                              'parent', tokens.main_window, ...
                              'draw_func', 'navbar', ...
                              'isi', recog_isi_dur, ...
                              'break', {recog_block_size, recog_break_dur}, ...
                              'trigger_next', false, ...
                              'stimmap', all_stim, ...
                              'prompt_dur', recog_dur, ...
                              'listen_dur', recog_listen_dur, ...
                              'default_respmap', recog_respmap, ...
                              'default_cresp', {}, ...
                              'device', tokens.user_keys);    

            eK_recog = startSession({eK_recog}, ...
                            'instruct',{instructRecog1,instructRecogGraphic}, ...
                            'info', {stimmap_targs, stimmap_novel}, ...
                            'seed', 'session', ...
                            'recovery_mode', recovery, ...
                            'params',params,'tokens',tokens);
            recog_stim = Shuffle([current_targs current_novel]);
            if ~isempty(eK_recog.trials)
                start_stim = eK_recog.trials.nesting{end};
                recog_stim = recog_stim(find(start_stim==recog_stim,1,'first'):end);
            end

            % loop over study trials
            for s = 1:length(recog_stim)
                
                % set condition
                this_stim = recog_stim{s};
                if any(strcmp(stimmap_targs.descriptors,this_stim))
                    cond = 'old';
                    cresp = {'remember','know'};
                else
                    cond = 'new';
                    cresp = {'new'};
                end
                
                % wait for response
                eK_recog = easyKeys(eK_recog, ...
                                    'stim', this_stim, ...
                                    'cond', {cond}, ...
                                    'cresp', cresp, ...
                                    'nesting', [block s], ...
                                    'draw_opts', {this_stim,[],[]});
            end
            endSession({eK_recog},'tokens',tokens,'params',params,'feedback',feedback,'message',end_task);
            
            
        %% multichoice object section
        case {'multiobject','multiobject practice'}
            
            % initialize
            eK_multiobject = initEasyKeys([params.session.sess_name ' - multiobject'], params.session.ppt_name, ...
                              'session', sess_num, ...
                              'num_trials', total_items, ...
                              'parent', tokens.main_window, ...
                              'draw_func', 'navbar', ...
                              'respmap_shuffle', 'shuffle_trial', ...
                              'isi', multiobject_isi_dur, ...
                              'break', {multiobject_block_size, multiobject_break_dur}, ...
                              'trigger_next', false, ...
                              'stimmap', all_stim, ...
                              'prompt_dur', multiobject_dur, ...
                              'listen_dur', multiobject_listen_dur, ...
                              'default_respmap', multiobject_respmap, ...
                              'default_cresp', {}, ...
                              'device', tokens.user_keys);    
            eK_multiobject = startSession({eK_multiobject}, ...
                            'instruct',{instructMultichoiceObject1, instructMultichoiceObjectGraphic}, ...
                            'info', {stimmap_targs, stimmap_novel}, ...
                            'seed', 'session', ...
                            'recovery_mode', recovery, ...
                            'params',params,'tokens',tokens);    
            all_scenes = Shuffle(all_scenes);
            if ~isempty(eK_multiobject.trials)
                start_scene = eK_multiobject.trials.nesting{end};
                all_scenes = all_scenes(find(start_scene==all_scenes,1,'first'):end);
            end
            
            % loop over study trials
            t_counter = 0;
            for s = all_scenes
                t_counter = t_counter + 1;
                
                % set condition
                avail = Shuffle(1:2);
                this_stim = scene_objs{s}{avail(1)};
                targ_stim = scene_objs{s}{avail(2)};
                
                % prepare respmap
                draw_respmap = multiobject_respmap;
                draw_respmap.descriptors = [{targ_stim} scene_multichoice_lures{s}]';
                stimorder = randperm(length(draw_respmap.descriptors));
                draw_respmap.descriptors = draw_respmap.descriptors(stimorder);
                cresp = draw_respmap.inputs(stimorder==1);
                
                % gather response
                eK_multiobject = easyKeys(eK_multiobject, ...
                                    'stim', this_stim, ...
                                    'cresp', cresp, ...
                                    'nesting', [block s], ...
                                    'draw_opts', {this_stim,draw_respmap,[]});
            end
            endSession({eK_multiobject},'tokens',tokens,'params',params,'feedback',feedback,'message',end_task);
            
        %% gross spatial configuration section
        case {'config', 'config practice', 'multichoice', 'multichoice practice'}

            % initialize
            eK_config = initEasyKeys([params.session.sess_name ' - config'], params.session.ppt_name, ...
                              'session', sess_num, ...
                              'num_trials', total_items, ...
                              'trigger_next', false, ...
                              'prompt_dur', config_dur, ...
                              'stimmap', all_stim, ...
                              'default_cresp', {}, ...
                              'device', tokens.user_keys);    

            switch session
                case {'config','config practice'}
                    long_instructs = {instructFlip1, instructFlipGraphic};
                    trial_bin{4} = {short_config_instructs,column_rect,col_wrap};
                    conds = {'correct','switched'};
                case {'multichoice','multichoice practice'}
                    long_instructs = {instructMultichoiceSpace1, instructMultichoiceSpaceGraphic};
                    trial_bin{4} = {short_multichoice_instructs,column_rect,col_wrap};
                    conds = {'cued','uncued'};
            end
            [eK_config] = startSession({eK_config}, ...
                            'instruct',long_instructs, ...
                            'info', {stimmap_targs, stimmap_novel}, ...
                            'seed', 'session', ...
                            'recovery_mode', recovery, ...
                            'params',params,'tokens',tokens);
            all_scenes = Shuffle(all_scenes);
            if ~isempty(eK_config.trials)
                start_scene = eK_config.trials.nesting{end};
                all_scenes = all_scenes(find(start_scene==all_scenes,1,'first'):end);
            end
            cond_vals = randperm(length(all_scenes));
            cond_vals = mod(cond_vals,2)+1;

            % loop over study trials
            t_counter = 0;
            for t = all_scenes
                t_counter = t_counter + 1;
                Screen('TextSize', tokens.main_window, params.appearance.size);
                isi(tokens.main_window, isi_dur, params.appearance.text);
                
                % take a break now and then
                relax(params, tokens, config_block_size, t, config_break_dur);
                
                % set object configuration based on targetness
                targ_search = strcmp(stimmap_targs.descriptors, scene_objs{t}{1});
                pair_search = strcmp(stimmap_targs.descriptors, scene_objs{t}{2});
                stimid_anchor = stimmap_targs.values(targ_search);
                stimid_pair = stimmap_targs.values(pair_search);
                switch session
                    case {'config','config practice'}
                        if strcmp(conds{cond_vals(t_counter)},'correct')
                            sample_bin{1}{1} = makeMap(scene_objs{t}(1:targs_per_scene), [find(targ_search) stimid_pair], squares{t}(1:targs_per_scene));
                            cresp = {'unswitched'};
                        else
                            sample_bin{1}{1} = makeMap(scene_objs{t}(1:targs_per_scene), [find(targ_search) stimid_pair], flipud(squares{t}(1:targs_per_scene)));
                            cresp = {'switched'};
                        end
                        sample_bin{1}{3} = 1:targs_per_scene;
                        respmap = config_respmap;
                    case {'multichoice','multichoice practice'}
                        object_files = empty_sq(1:max_multichoice_options);
                        object_pos = [squares{t}(2); lure_squares{t}(1:max_multichoice_options-1)];
                        order = randperm(length(object_files));
                        object_files = object_files(order);
                        resp_keys = multichoice_resps(order);
                        cresp = resp_keys(1);
                        respmap = multichoice_respmap;
                        
                        if strcmp(conds{cond_vals(t_counter)},'cued')
                            sample_bin{1}{1} = makeMap([scene_objs{t}(1) scene_objs{t}(2) object_files],[stimid_anchor stimid_pair zeros(1,length(object_files))], [squares{t}(1) multichoice_ref_pos object_pos']);
                            sample_bin{1}{3} = 1:targs_per_scene+max_multichoice_options;
                        else
                            sample_bin{1}{1} = makeMap([scene_objs{t}(2) object_files],[stimid_pair zeros(1,length(object_files))], [multichoice_ref_pos object_pos']);
                            sample_bin{1}{3} = 1:targs_per_scene+max_multichoice_options-1;
                        end
                end
                
                % draw it
                Screen('TextSize', tokens.main_window, params.appearance.size+font_diff);
                drawFunc_object(tokens.main_window, [], draw_func_args, trial_bin, sample_bin);
                onset = Screen('Flip', tokens.main_window);

                % wait for response
                eK_config = easyKeys(eK_config, ...
                                        'stim', num2str(t), ...
                                        'cond', conds{cond_vals(t_counter)}, ...
                                        'cresp', cresp, ...
                                        'respmap', respmap, ...
                                        'onset', onset, ...
                                        'info', {scene_objs{t}, squares{t}}, ...
                                        'nesting', [block t]);
            end % study trials
            endSession({eK_config},'tokens',tokens,'params',params,'feedback',feedback,'message',end_task);

        
        %% test section
        case {'drag composite', 'drag composite practice', 'drag global detail', 'drag global detail practice', 'drag targonly', 'drag item', 'drag item practice', 'drag item cued', 'drag item detail', 'drag global detail', 'drag local detail', 'motor', 'motor practice'}

            % initialize
            eS = initEasyScale([params.session.sess_name ' - test'], params.session.ppt_name, input_func, ...
                              'input_func_device', tokens.cursor_device, ...
                              'session', sess_num, ...
                              'num_trials', total_items, ...
                              'draw_func', draw_func, ...
                              'score_func', score_func, ...
                              'parent', tokens.main_window, ...
                              'break', {drag_block_size, drag_break_dur}, ...
                              'prompt_dur', test_dur, ...
                              'stimmap', all_stim, ...
                              'default_draw_func_args', draw_func_args, ...
                              'default_input_func_args', input_func_args, ...
                              'default_score_func_args', score_func_args);

            % allocate instructions based on test type
            switch session
                case {'drag composite','drag composite practice'}
                    long_instruct = {instructCompositeTest1,instructCompositeGraphic};
                case {'drag targonly','drag targonly practice'}
                    long_instruct = {instruct2targs1,instruct2targsGraphic};
                case {'drag global detail','drag global detail practice'}
                    long_instruct = {instructGlobalDetail1,instructGlobalGraphic};
                case {'drag local detail','drag item detail'}    
                    long_instruct = {instructDetail1,instructDetailGraphic};
                case {'drag item', 'drag item practice'}
                    long_instruct = {instructItem1,instructItemGraphic};
                case {'drag item cued', 'drag item cued practice'}
                    long_instruct = {instructItemCued1,instructItemCuedGraphic};
                case {'motor','motor practice'}
                    long_instruct = {instructMotor1, instructMotorGraphic};
            end
            [eS] = startSession({eS}, ...
                            'instruct',long_instruct, ...
                            'info', {stimmap_targs, stimmap_novel}, ...
                            'seed', 'session', ...
                            'recovery_mode', recovery, ...
                            'params',params,'tokens',tokens);
            all_scenes = Shuffle(all_scenes);
            if ~isempty(eS.trials)
                start_scene = eS.trials.nesting{end};
                all_scenes = all_scenes(find(start_scene==all_scenes,1,'first'):end);
            end

            % loop over test trials
            for t = all_scenes
                Screen('TextSize', tokens.main_window, params.appearance.size);
                isi(tokens.main_window, isi_dur, params.appearance.text);

                % get targets
                switch session
                    case {'drag composite','drag composite practice'}
                        object_files = [scene_objs{t}(1:targs_per_scene) scene_drag_lures{t}(1:2)];
                        target = [ones(1,targs_per_scene) zeros(size(scene_drag_lures{t}(1:2)))];
                        order = randperm(length(target));
                        instruct = short_test_instructs;
                    case {'drag targonly','drag item','drag item practice','drag item cued'}
                        if any(strcmp({'drag item','drag item practice'},session)), objs_to_get = 1;
                        else objs_to_get = 2;
                        end
                        instruct = short_item_instructs;
                        object_files = scene_objs{t}(1:objs_to_get);
                        target = ones(1,objs_to_get);
                        order = randperm(objs_to_get);
                    case {'drag local detail', 'drag global detail','drag global detail practice','drag item detail'}
                        if any(strcmp({'drag item detail'}, session))
                            objs_to_get = 1;
                            instruct = short_item_instructs;
                        else
                            objs_to_get = 2;
                            instruct = short_detail_instructs;
                        end
                        object_files = scene_objs{t}(1:objs_to_get);
                        target = ones(1,objs_to_get);
                        order = 1:length(object_files);
                    case {'motor','motor practice'}
                        object_files = [scene_objs{t}(1:targs_per_scene) scene_drag_lures{t}(1:2)];
                        target = [ones(1,targs_per_scene) zeros(size(scene_drag_lures{t}(1:2)))];
                        order = randperm(length(target));
                        instruct = short_motor_instructs;
                end
                object_files = object_files(order);
                target = target(order);
                trial_bin{4} = {instruct, column_rect, col_wrap};
                
                % identify needed objects / images
                for o = 1:length(object_files)
                    targ_search = strcmp(stimmap_targs.descriptors,object_files{o});
                    lure_search = strcmp(stimmap_novel.descriptors,object_files{o});
                    if any(targ_search)
                        object_vals(o) = stimmap_targs.values(targ_search);
                        object_pos(o) = stimmap_targs.inputs(targ_search);
                    else
                        object_vals(o) = stimmap_novel.values(lure_search);
                        object_pos(o) = {[nan nan]};
                    end
                end
                trial_bin{3} = [];

                switch session
                    % for the detail tests, we start out approximately in the right spot,
                    % but add noise
                    case {'drag global detail','drag global detail practice'}
                        global_offset = 0;
                        while global_offset < global_var/2
                            global_offset = normrnd(0,global_var,1,2);
                        end
                        for o = 1:length(object_files)
                            start_pos{o} = object_pos{o}(1:2) + global_offset;
                        end
                        trial_bin{3} = {object_vals}; % lock all objects together
                        
                    case {'drag local detail','drag local detail practice'}
                        for o = 1:length(object_files)
                            local_offset = 0;
                            while local_offset < local_var/2
                                local_offset = normrnd(0,local_var,1,2);
                            end
                            start_pos{o} = object_pos{o}(1:2) + local_offset;
                        end
    
                    % for cued item test, one of the items is placed in its correct location
                    case {'drag item cued','drag item cued practice'}
                        start_pos{1} = [test_obj_start_col test_obj_start_row(1)];
                        start_pos{2} = object_pos{o}(1:2);
                        
                    % but normally, objects are placed in the left-handed column to start
                    otherwise
                        for o = 1:length(object_files)
                            start_pos{o} = [test_obj_start_col test_obj_start_row(o)];
                        end
                end
                
                % invert object_vals to designate lures
                object_vals(~target) = -1 * object_vals(~target);

                % display a faded copy of the destination points if we are in motor mode
                switch session
                    case {'motor','motor practice'}
                        for o = 1:length(object_files)
                            if object_vals(o) > 0
                                if ~any(isnan(object_pos{o}))
                                    motor.obj_rect{o} = object_pos{o};
                                    obj_img = prepareImage(object_files{o}, 'alpha', thresh);
                                    obj_img(:,:,4) = obj_img(:,:,4) * .33;
                                    motor.object_tex{o} = Screen('MakeTexture', tokens.main_window, obj_img);
                                    trial_bin{5} = motor;
                                end
                            end
                        end
                        clear motor;
                    case {'drag item cued','drag item cued practice'}
                        item.obj_rect{o} = object_pos{2};
                        obj_img = prepareImage(object_files{2}, 'alpha', thresh);
                        item.object_tex{o} = Screen('MakeTexture', tokens.main_window, obj_img);
                        trial_bin{5} = item;
                        start_pos(2) = []; object_vals(2) = []; object_files(2) = [];
                        clear item;
                    otherwise trial_bin{5} = [];
                end
                
                % place objects and the cursor into their computed starting positions
                cursor_start = [trial_area_pix/2 + trial_area_rect(1:2) 0 0 nan]; % init vals for xpos ypos l-click r-click dragging
                for o = 1:length(object_files)
                    start_pos{o} = [start_pos{o}-obj_size_half start_pos{o}+obj_size_half];
                    cursor_start = [cursor_start start_pos{o}(1:2)]; % cursor also within bounds
                end
                respmap = makeMap(object_files, object_vals, start_pos); %squares{t});
                
                % Start the dragging while gathering data
                eS = easyScale(eS, ...
                                      'stim', num2str(t), ...
                                      'respmap', respmap, ...
                                      'nesting', [block t], ...
                                      'init_val', cursor_start, ...
                                      'trial_bin', trial_bin, ...
                                      'cresp', NaN);
            end % test trials
            endSession({eS},'tokens',tokens,'params',params,'feedback',feedback,'message',end_task);
            
        otherwise
            error('unrecognized session type');
    end % session switch

return

function relax(params, tokens, block_size, trial, break_dur)

    % break on appropriate trials
    if ~(mod(trial,block_size))
        % prompt for break
        if ischar(break_dur) || isinf(break_dur)
            DrawFormattedText(tokens.main_window,['-- BREAK --\n\nKeep up the great work! Press ' ...
                params.keys.user.trigger ' once you''re ready to continue.'],'center','center',params.appearance.text);
            Screen(tokens.main_window,'Flip');
            waitForKeyboard(params.keys.user.trigger,tokens.user_keys);
        else
            DrawFormattedText(tokens.main_window,['-- BREAK --\n\nKeep up the great work! The task will ' ...
                'resume in ' num2str(break_dur) ' seconds.'],'center','center',params.appearance.text);
            Screen(tokens.main_window,'Flip');
            WaitSecs(break_dur);
        end
        
        % resume task
        DrawFormattedText(tokens.main_window, '+', 'center', 'center', params.appearance.text);
        Screen(tokens.main_window,'Flip');
        WaitSecs(1);
    end
return